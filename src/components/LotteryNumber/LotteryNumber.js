import React, {Component} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './LotteryNumber.css';
import { Container, Button, Col } from 'reactstrap';

class LotteryNumber extends Component {

	state = {
		arr: []
	};

	getRandom = () => {
		let newArray = [];

		for (let i = 0; i < 5;) {
			let random = Math.floor(Math.random() * (37 - 5)) + 5;
			if (!newArray.includes(random)) {
				newArray.push(random);
				i ++;
			}
		}
		newArray.sort((x,y) => x - y);
		this.setState({arr:newArray});
	};

	render() {
		return (
			<Container>
				<Col md={{ size: 6, offset: 3 }}>
					<h1>LOHOTRON!!!</h1>
					<Button color="success" onClick={this.getRandom}>Lottery numbers!</Button>
					<br/>
					<div className="title">Here is your random numbers:</div>
					<ul id="NumberList">
						{this.state.arr.map((number, key) => <li key={key}>{number}</li>)}
					</ul>
				</Col>
			</Container>
		);
	}
}





export default LotteryNumber;